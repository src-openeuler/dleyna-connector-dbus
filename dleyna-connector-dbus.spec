%global __provides_exclude_from ^%{_libdir}/dleyna-1.0/connectors/.*\\.so$
Name:                dleyna-connector-dbus
Version:             0.3.0
Release:             4
Summary:             D-Bus connector for dLeyna services
License:             LGPLv2
URL:                 https://01.org/dleyna/
Source0:             https://01.org/sites/default/files/downloads/dleyna/dleyna-connector-dbus-%{version}.tar_2.gz
BuildRequires:       dbus-devel pkgconfig(dleyna-core-1.0) pkgconfig(gio-2.0) pkgconfig(glib-2.0)
BuildRequires:       pkgconfig autoconf automake libtool
%description
dLeyna services D-Bus connector.

%package             devel
Summary:             Development files for dleyna-connector-dbus
Requires:            dleyna-connector-dbus = %{version}-%{release}
%description         devel
The dleyna-connector-dbus-devel package contains libraries and header files for
developing applications that use dleyna-connector-dbus.

%prep
%autosetup -n dleyna-connector-dbus-%{version} -p1

%build
autoreconf -f -i
%configure --disable-silent-rules 
%make_build

%install
%make_install
%delete_la

%files
%doc AUTHORS ChangeLog  README COPYING
%{_libdir}/dleyna-1.0/connectors/libdleyna-connector-dbus.so

%files devel
%{_libdir}/pkgconfig/dleyna-connector-dbus-1.0.pc

%changelog
* Mon Apr 27 2020 yanan li <liyanan032@huawei.com> - 0.3.0-4
- Package init
